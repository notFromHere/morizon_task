module.exports = {
  extends: [
    'stylelint-config-standard',
  ],
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  rules: {
    'color-hex-case': null,
    'selector-class-pattern': null,
    'selector-max-compound-selectors': null,
    'max-nesting-depth': [3, {
      ignore: ['pseudo-classes'],
    }],
  },
};
