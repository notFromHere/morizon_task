import gql from 'graphql-tag';

const SEARCH_PROPERTIES_FILTERS = gql`
  query searchProperties($pageNumber: Int, $priceFrom: String, $priceTo: String,
    $numberOfRoomsFrom: Int, $numberOfRoomsTo: Int, $numberOfResult: Int){
    searchProperties(searchFilters: {
      priceFrom: $priceFrom,
      priceTo: $priceTo,
      numberOfRoomsFrom: $numberOfRoomsFrom,
      numberOfRoomsTo: $numberOfRoomsTo,
    }, searchOrder: {
      sortKey: PRICE,
      sortOrder: ASC,
    }, pageNumber: $pageNumber,
      numberOfResults: $numberOfResult){
      totalCount
      location {
        name
        type
        nameFull
      }
      nodes {
        locationShort
        price {
          amount
          currency
        }
        numberOfRooms
        totalArea
      }
    }
  }`;

export {
  SEARCH_PROPERTIES_FILTERS,
};
